export const REGEXP_EM = /__(.*?)__/g;
export const REGEXP_STRONG = /\*(.*?)\*/g;
export const REGEXP_CODE = /`(.*?)`/g;

export const format = source =>
  source
    .replace(REGEXP_EM, '<em>$1</em>')
    .replace(REGEXP_STRONG, '<strong>$1</strong>')
    .replace(REGEXP_CODE, '<code>$1</code>');
